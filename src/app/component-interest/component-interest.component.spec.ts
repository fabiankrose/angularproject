import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentInterestComponent } from './component-interest.component';

describe('ComponentInterestComponent', () => {
  let component: ComponentInterestComponent;
  let fixture: ComponentFixture<ComponentInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
